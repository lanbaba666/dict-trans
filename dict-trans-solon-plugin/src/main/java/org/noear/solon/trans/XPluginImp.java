package org.noear.solon.trans;

import lombok.extern.slf4j.Slf4j;
import org.noear.solon.core.AppContext;
import org.noear.solon.core.Plugin;
import org.noear.solon.trans.config.TranslatorConfig;

/**
 * @author 等風來再離開
 * @date 2023/6/25 14:40
 **/
@Slf4j
public class XPluginImp implements Plugin {
    @Override
    public void start(AppContext context) throws Throwable {
        context.beanMake(TranslatorConfig.class);
        log.info("dict trans 插件加载成功....");
    }
}
