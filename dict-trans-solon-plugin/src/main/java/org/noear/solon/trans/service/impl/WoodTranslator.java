package org.noear.solon.trans.service.impl;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.lang.Opt;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.aizuda.trans.annotation.Dictionary;
import com.aizuda.trans.entity.ExtendParam;
import com.aizuda.trans.enums.FormatType;
import com.aizuda.trans.service.Translatable;
import com.aizuda.trans.util.NameUtil;
import lombok.extern.slf4j.Slf4j;
import org.noear.solon.annotation.Component;
import org.noear.wood.DataItem;
import org.noear.wood.DataList;
import org.noear.wood.DbContext;
import org.noear.wood.DbTableQuery;
import org.noear.wood.annotation.Table;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 翻译默认实现
 * <p>
 * 适用于字典为单表，使用sql查询
 *
 * @author 蓝健
 * @create 2023-06-26
 */
@Slf4j
@Component
public class WoodTranslator implements Translatable {


    @Override
    public List<Object> translate(String origin, Dictionary dictConfig, ExtendParam extendParam) {
        // 获取参数
        final String codeColumn = dictConfig.codeColumn();
        final String[] textColumnArray = dictConfig.textColumn();
        final String groupColumn = dictConfig.groupColumn();
        final Class dictClass = extendParam.getDictClass();
        final String groupValue = extendParam.getGroupValue();
        final String simpleName = dictClass.getSimpleName();
        final String tableName = getTableName(dictConfig, dictClass);

        Assert.isTrue(StrUtil.isNotBlank(codeColumn), "@Dictionary注解codeColumn配置有误，找不到指定的属性名，class: {}", simpleName);
        Assert.isTrue(ArrayUtil.isNotEmpty(textColumnArray), "@Dictionary注解textColumn配置有误，找不到指定的属性名，class: {}", simpleName);

        List<Object> rsList = new ArrayList<>(textColumnArray.length);
        try {
            log.debug("---> 触发字典翻译：查询表 {} 中的字段 {} ，查询条件 {} = {}", tableName, textColumnArray, codeColumn, origin);

            // 查询条件为空时直接返回相对应个数的空数组
            if (ObjectUtil.isNull(origin) || StrUtil.isBlank(origin)) {
                log.debug("---> 触发字典翻译：查询条件为空，直接返回对应个数的空数组");
                return Opt.ofNullable(textColumnArray).stream().map(s -> StrUtil.EMPTY).collect(Collectors.toList());
            }
            DbTableQuery where = DbContext.use("db1").table(tableName).whereTrue();
            if (StrUtil.isNotEmpty(groupColumn)) {
                where.andEq(groupColumn, groupValue);
            }

            if (StrUtil.contains(origin, StrUtil.COMMA)) {
                // 多条记录取单列（查询结果为多条记录，循环后转为单条）
                // 传入数据：1,2,3
                // 查询结果：["张三","李四","王五"]
                // 返回结果：张三、李四、王五
                final String field = textColumnArray[0];
                where.andIn(codeColumn, StrUtil.split(origin, StrUtil.COMMA));
                DataList entityList = where.selectDataList(String.join(",", textColumnArray));
                rsList.add(entityList.getMapList().stream().map(e -> e.get(field).toString()).collect(Collectors.joining(StrUtil.COMMA)));
            } else {
                // 单条记录多列（查询结果为单条多列，循环后为多条，根据传入的字段顺序返回）
                // 传入数据：1
                // 查询结果：{ "列1": "aa", "列2": "bb"}
                // 返回结果：["aa","bb"]
                where.andEq(codeColumn, origin);
                DataItem entity = where.selectDataItem(String.join(",", textColumnArray));
                for (String column : textColumnArray) {
                    rsList.add(entity.getString(column));
                }
            }
        } catch (NullPointerException e) {
            // 空指针异常时，可能查询条件为空。返回空数组
            log.error("---> DataBaseTranslator 字典翻译，空指针异常", e);
        } catch (SQLException e) {
            log.error("---> DataBaseTranslator 字典翻译，SQL查询异常", e);
        }

        return rsList;
    }

    /**
     * 获取表名
     *
     * @param dictConfig dict配置
     * @param dictClass  dict类
     * @return {@link String }
     */
    private String getTableName(Dictionary dictConfig, Class dictClass) {
        if (StrUtil.isNotEmpty(dictConfig.table())) {
            return dictConfig.table();
        }
        // 类名转表名
        final String className = dictClass.getSimpleName();
        String tName = className.charAt(0) + NameUtil.parseCamelTo(className.substring(1), FormatType.UPPERCASE_UNDERLINE);
        // 获取wood注解上的表名
        Table tableName = (Table) dictClass.getAnnotation(Table.class);
        if (null != tableName) {
            tName = tableName.value();
        }
        return tName;
    }

}
