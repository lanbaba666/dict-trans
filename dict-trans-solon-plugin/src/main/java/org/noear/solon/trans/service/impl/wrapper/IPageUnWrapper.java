package org.noear.solon.trans.service.impl.wrapper;


import org.noear.solon.trans.service.UnWrapper;
import org.noear.wood.IPage;

/**
 * mybatis-plus 的 IPage 解包器
 *
 * @author nn200433
 * @date 2023-05-25 025 10:02:45
 */
public class IPageUnWrapper<T> implements UnWrapper<IPage<T>> {

    @Override
    public Object unWrap(IPage<T> source) {
        return source.getList();
    }


}
