package org.noear.solon.trans.config;


import com.aizuda.trans.service.DictTranslateService;
import com.aizuda.trans.service.SummaryExtractService;
import com.aizuda.trans.service.WriteFieldNameService;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Condition;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.trans.service.defaults.DefaultWriteFieldNameServiceImpl;
import org.noear.solon.trans.service.impl.DefaultDictTranslateServiceImpl;
import org.noear.solon.trans.service.impl.DefaultSummaryExtractServiceImpl;
import org.noear.solon.trans.service.impl.wrapper.IPageUnWrapper;
import org.noear.solon.trans.util.TranslatorUtil;

/**
 * 翻译配置
 *
 * @author nn200433
 * @date 2022-08-18 018 16:37:42
 */
@Configuration
public class TranslatorConfig {

    /**
     * 注册字典翻译服务默认实现
     *
     * @return {@link DictTranslateService }
     * @author nn200433
     */
    @Bean
    @Condition(onMissingBean = DictTranslateService.class)
    public DictTranslateService dictTranslateService() {
        return new DefaultDictTranslateServiceImpl();
    }

    /**
     * 注册摘要提取服务默认实现
     *
     * @return {@link DictTranslateService }
     * @author nn200433
     */
    @Bean
    @Condition(onMissingBean = SummaryExtractService.class)
    public SummaryExtractService summaryExtractService() {
        return new DefaultSummaryExtractServiceImpl();
    }


    /**
     * 注册IPage解包器
     *
     * @return {@link IPageUnWrapper }<{@link Object }>
     * @author nn200433
     */
//    @Bean(typed = true)
//    public IPageUnWrapper<Object> iPageUnWrapper() {
//        return new IPageUnWrapper<Object>();
//    }

    /**
     * 翻译工具类
     *
     * @return {@link TranslatorUtil }
     * @author nn200433
     */
    @Bean
    public TranslatorUtil translatorUtil() {
        return new TranslatorUtil();
    }


    /**
     * 注册写入字段服务默认实现
     *
     * @return {@link WriteFieldNameService }
     * @author nn200433
     */
    @Bean
    @Condition(onMissingBean = WriteFieldNameService.class)
    public WriteFieldNameService writeFieldNameService() {
        return new DefaultWriteFieldNameServiceImpl();
    }
}
