package org.noear.solon.trans.service.impl;

import cn.hutool.json.JSONUtil;
import com.aizuda.trans.annotation.Dictionary;
import com.aizuda.trans.entity.ExtendParam;
import com.aizuda.trans.service.Translatable;
import org.noear.solon.annotation.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 等風來再離開
 * @date 2023/6/26 16:47
 **/
@Component
public class UserTranslateServiceImpl implements Translatable {
    @Override
    public List<Object> translate(String origin, Dictionary dictConfig, ExtendParam extendParam) {
        System.out.println("user 自定义翻译--->" + origin);
        System.out.println("user dictConfig--->" + JSONUtil.toJsonStr(dictConfig));
        System.out.println("user extendParam--->" + JSONUtil.toJsonStr(extendParam));

        String conditionValue = extendParam.getConditionValue();

        System.out.println("conditionValue--->" + conditionValue);
        List<Object> list = new ArrayList<>();
        list.add("123");
        return (list);
    }
}
