package org.noear.solon.trans.util;


import cn.hutool.core.convert.Convert;
import org.noear.solon.trans.handler.TranslatorHandle;

/**
 * 翻译工具类
 *
 * @author nn200433
 * @date 2023-08-01  16:28:54
 */
public class TranslatorUtil {


    /**
     * 翻译
     * <p>
     * 会先进行解对象（比如 Ipage 对象只会提取 Records 进行翻译）
     *
     * @param origin 源对象
     * @author nn200433
     */
    public static void transform(Object origin) {

        TranslatorHandle.transform(Convert.convert(Object.class, origin));
    }

}
