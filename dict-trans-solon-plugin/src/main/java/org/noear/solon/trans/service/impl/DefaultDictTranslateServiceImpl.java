package org.noear.solon.trans.service.impl;

import com.aizuda.trans.service.DictTranslateService;
import org.noear.solon.annotation.Component;

/**
 * 默认字典翻译服务实现
 *
 * @author nn200433
 * @date 2022-08-18 018 16:32:24
 */
@Component
public class DefaultDictTranslateServiceImpl implements DictTranslateService {

    @Override
    public String findDictLabel(String dictCode, String dictValue) {
        return dictValue;
    }

}
