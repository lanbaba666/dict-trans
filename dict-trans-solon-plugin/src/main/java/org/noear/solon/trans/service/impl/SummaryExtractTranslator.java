package org.noear.solon.trans.service.impl;

import com.aizuda.trans.annotation.Dictionary;
import com.aizuda.trans.entity.ExtendParam;
import com.aizuda.trans.service.SummaryExtractService;
import com.aizuda.trans.service.Translatable;
import org.noear.solon.Solon;
import org.noear.solon.annotation.Component;

import java.util.Collections;
import java.util.List;

/**
 * 摘要提取翻译
 *
 * @author nn200433
 * @date 2023-06-07 09:32:49
 */
@Component
public class SummaryExtractTranslator implements Translatable {


    @Override
    public List<Object> translate(String origin, Dictionary dictConfig, ExtendParam extendParam) {
        return Collections.singletonList(Solon.context().getBean(SummaryExtractService.class).extract(origin, extendParam.getMaxLen()));
    }

}
