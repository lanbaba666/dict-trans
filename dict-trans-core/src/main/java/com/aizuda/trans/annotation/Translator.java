package com.aizuda.trans.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 【字典翻译注解】指定在方法上或字段上，对方法返回值或者嵌套字段进行翻译
 *
 * @author luozhan
 * @date 2020-04
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE})
public @interface Translator {

}
