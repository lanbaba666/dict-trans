package com.aizuda.trans.annotation;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * 注解代理生成器
 *
 * @author nn200433
 * @date 2024-01-29 01:41:17
 */
public class AnnotationProxyBuilder implements InvocationHandler {

    private final Map<String, Object> values;

    public AnnotationProxyBuilder(Map<String, Object> values) {
        this.values = values;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String methodName = method.getName();
        if (values.containsKey(methodName)) {
            return values.get(methodName);
        }
        return method.getDefaultValue();
    }

}
