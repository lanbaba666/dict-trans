package com.aizuda.trans.config;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ClassUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.noear.solon.Solon;
import org.noear.solon.annotation.Component;
import org.noear.solon.core.aspect.Interceptor;
import org.noear.solon.core.aspect.InterceptorEntity;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.Filter;
import org.noear.solon.core.handle.FilterChain;
import org.noear.solon.core.handle.Result;

import java.util.List;

@Component
@Slf4j
public class AppFilter implements Filter {

    @Override
    public void doFilter(Context c, FilterChain chain) throws Throwable {
        // 1. 开始计时
        long startTime = System.currentTimeMillis();

        chain.doFilter(c);
        if ("WEBSOCKET".equals(c.method())) {
            return;
        }
        if (!c.getHandled()) {
            c.status(404);
        }
        if (!c.getHandled() || c.status() == 404) {
            c.setHandled(true);
            c.setRendered(true);
            c.status(404);
            log.debug("地址" + c.path() + "不存在");
            c.outputAsJson(JSONUtil.toJsonStr(Result.failure(404, c.path() + "页面不存在")));
            return;
        }

        // 开发模式才输出参数
        if (Solon.cfg().isDebugMode()) {
            report(c, startTime);
        }

    }

    @SneakyThrows
    public void report(Context context, Long time) {
        if (context.controller() == null) {
            return;
        }
        String controllerPath = context.controller().getClass().getName();
        CtClass ctClass = ClassPool.getDefault().get(controllerPath);
        String methodName = context.action().method().getName().replace("/", "");
        CtMethod declaredMethod = ctClass.getDeclaredMethod(methodName);
        int lineNumber = declaredMethod.getMethodInfo().getLineNumber(0) - 1;
        String controllerName =
                ".(" + ClassUtil.getShortClassName(controllerPath) + ".java:" + lineNumber + ")";

        StringBuilder logInfo = new StringBuilder();
        logInfo.append("\n");
        logInfo.append("+ Solon-").append(Solon.version()).append("  ").append(DateUtil.now())
                .append(" action report ").append("======================================").append("\n");
        logInfo.append("| Request           : ").append(context.method()).append(" ")
                .append(context.path()).append("\n");
        logInfo.append("| Request IP        : ").append(context.realIp()).append("\n");
        logInfo.append("| Controller        : \n                      ").append(controllerPath)
                .append(controllerName).append("\n");
        logInfo.append("| Interceptor       : ").append("\n                      ");

        List<InterceptorEntity> interceptors = context.action().method().getInterceptors();
        for (int i = 0; i < interceptors.size(); i++) {
            Interceptor interceptor = interceptors.get(i).getReal();
            String name = interceptor.getClass().getName();
            logInfo.append(name).append(".(").append(ClassUtil.getShortClassName(name))
                    .append(".java:1)");
            if (i == interceptors.size() - 1) {
                logInfo.append("\n");
            } else {
                logInfo.append("\n                      ");
            }
        }
        logInfo.append("| Method            : ").append(methodName).append("()").append("\n");
        logInfo.append("| Elapsed Time      : ").append(System.currentTimeMillis() - time).append("ms")
                .append("\n");

        if (MapUtil.isNotEmpty(context.paramMap())) {
            logInfo.append("| Request Params    : ");
            logInfo.append(context.paramMap());
        } else if (StrUtil.isNotBlank(context.body())) {
            logInfo.append("| Request BodyParams : ");

            if (context.body().length() >= 100) {
                logInfo.append(StrUtil.replace(context.body(), 100, context.body().length() - 1, "...."));
            } else {
                logInfo.append(context.body());
            }


        }
        logInfo.append("\n");
        logInfo.append(
                "+========================================== End ===========================================\n");
        System.out.println(logInfo);
    }


}