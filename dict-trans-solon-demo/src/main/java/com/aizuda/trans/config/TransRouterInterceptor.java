package com.aizuda.trans.config;

import cn.hutool.json.JSONUtil;
import com.aizuda.trans.annotation.Translator;
import lombok.extern.slf4j.Slf4j;
import org.noear.solon.Solon;
import org.noear.solon.annotation.Component;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.Handler;
import org.noear.solon.core.handle.Result;
import org.noear.solon.core.route.RouterInterceptor;
import org.noear.solon.core.route.RouterInterceptorChain;
import org.noear.solon.trans.handler.TranslatorHandle;

/**
 * @author 等風來再離開
 * @date 2023/6/25 15:43
 **/
@Component
@Slf4j
public class TransRouterInterceptor implements RouterInterceptor {


    @Override
    public void doIntercept(Context ctx, Handler mainHandler, RouterInterceptorChain chain) throws Throwable {
        chain.doIntercept(ctx, mainHandler);
    }


    /**
     * 提交结果（ render 执行前调用）//不要做太复杂的事情
     */
    @Override
    public Object postResult(Context ctx, Object result) throws Throwable {
        if (ctx.errors != null) {
            ctx.errors.printStackTrace();
            log.error("全局异常{}", ctx.errors.getMessage());
            Throwable throwable = ctx.errors;
            if (Solon.cfg().isDebugMode()) {
                throwable.getStackTrace();
            }
            String errorMessage = throwable.getMessage();
            if (throwable.getStackTrace().length > 0) {
                errorMessage = throwable.getClass() + "：" + throwable.getMessage();
            }

            return Result.failure("系统异常，请稍后再试！" + errorMessage);

        }


        //提示：此处只适合做结果类型转换
        Translator translator = ctx.action().method().getAnnotation(Translator.class);
        if (translator != null && result != null && !(result instanceof Throwable) && ctx.action() != null) {
            // todo 根据自己项目的响应结果类，修改Result
            if (result instanceof com.aizuda.trans.entity.Result) {
                com.aizuda.trans.entity.Result result1 = (com.aizuda.trans.entity.Result) result;
                // 先从容器中获取的转换器进行返回值解包，注意此处返回结果可能是Bean也可能是集合。然后再进行翻译
                System.out.println("翻译RESULT " + JSONUtil.toJsonStr(result1.getData()));
                TranslatorHandle.transform(result1.getData());

            } else {
                System.out.println("翻译" + JSONUtil.toJsonStr(result));
                TranslatorHandle.transform(result);

            }
        }

        return result;
    }
}
