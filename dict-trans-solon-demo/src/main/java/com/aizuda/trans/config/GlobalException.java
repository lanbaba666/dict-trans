package com.aizuda.trans.config;


import cn.hutool.json.JSONUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.noear.solon.annotation.Component;
import org.noear.solon.core.event.EventListener;
import org.noear.solon.core.handle.Context;
import org.noear.solon.core.handle.Result;

import java.security.SignatureException;
import java.sql.SQLException;


/**
 * 全局异常
 *
 * @author 等風來再離開
 * @date 2022/11/30
 */
@Slf4j
@Component
public class GlobalException implements EventListener<Throwable> {


    @SneakyThrows
    @Override
    public void onEvent(Throwable throwable) {
        String errorClass = "";
        String errorMessage = throwable.getMessage();
        if (throwable.getStackTrace().length > 0) {
            StackTraceElement stackTraceElement = throwable.getStackTrace()[0];
            errorClass = stackTraceElement.getClassName() + "：" + stackTraceElement.getLineNumber();
            errorMessage = throwable.getClass() + "：" + throwable.getMessage();
        }
        throwable.printStackTrace();
        Context c = Context.current();
        log.error("发生异常：{}\n{}", errorClass, errorMessage);
        if (null != c) {
            c.status(500);
            c.setHandled(true);
            c.setRendered(true);
            Result ret;
            if (throwable instanceof SQLException) {
                SQLException exception = (SQLException) throwable;
                ret = Result.failure("数据库发生错误:" + exception.getMessage());
            } else if (throwable instanceof SignatureException) {
                ret = Result.failure(401, "token失效");
            } else {
                ret = Result.failure("系统异常,请稍后再试！" + errorMessage);
            }
            c.outputAsJson(JSONUtil.toJsonStr(ret));
        }

    }
}
