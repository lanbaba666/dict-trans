package com.aizuda.trans.config;

import com.zaxxer.hikari.HikariDataSource;
import org.noear.solon.annotation.Bean;
import org.noear.solon.annotation.Configuration;
import org.noear.solon.annotation.Inject;

import javax.sql.DataSource;

/**
 * @author 等風來再離開
 * @date 2023/6/25 16:06
 **/
@Configuration
public class AppConfig {


    @Bean(name = "db1", typed = true)
    public DataSource db1(@Inject("${datasource}") HikariDataSource ds) {
        return ds;
    }
}
