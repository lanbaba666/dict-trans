package com.aizuda.trans.entity;

import com.aizuda.trans.annotation.Dictionary;
import org.noear.wood.annotation.Table;

/**
 * 用户(数据库)
 *
 * @author nn200433
 * @date 2022-12-16 016 14:07:27
 */
@Dictionary(codeColumn = "id", textColumn = {"user_name"})
@Table("sys_user")
public class UserDB {


    private String id;


    private String name;

}
