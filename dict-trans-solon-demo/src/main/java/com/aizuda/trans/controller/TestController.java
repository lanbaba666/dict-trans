package com.aizuda.trans.controller;

import cn.hutool.core.lang.Console;
import cn.hutool.json.JSONUtil;
import com.aizuda.trans.annotation.Translator;
import com.aizuda.trans.demo.DemoService;
import com.aizuda.trans.entity.*;
import com.aizuda.trans.service.DictTranslateService;
import lombok.extern.slf4j.Slf4j;
import org.noear.solon.annotation.Controller;
import org.noear.solon.annotation.Get;
import org.noear.solon.annotation.Inject;
import org.noear.solon.annotation.Mapping;

import java.sql.SQLException;
import java.util.List;

@Slf4j
@Controller
@Mapping("/test")
public class TestController {


    @Inject
    DictTranslateService dictTranslateService;

    @Inject
    DemoService demoService;

    @Get
    @Mapping("/test")
    @Translator
    public Result test() {
        System.out.println("原始数据:::" + JSONUtil.toJsonStr(demoService.responseNestedMock()));
        return Result.builder().status(200).data(demoService.responseNestedMock()).build();
    }


    @Get
    @Mapping("/test2")
    @Translator
    public Result test2() {

        People people = new People();

        people.setId("1");

        return Result.builder().status(200).data(people).build();
    }


    @Mapping("/page")
    @Translator
    public Result page() throws SQLException {


        //   List<Map<String, Object>> sysUser = dbContext.table("sys_user").paging(0, 10).selectMapList("*");

        People2 userDB = new People2();
        userDB.setId("1");

        return Result.builder().status(200).data(userDB).build();
    }


    @Get
    @Mapping("/test3")
    @Translator
    public Result test3(String code, String value) {
        return Result.builder().status(200).data(dictTranslateService.findDictLabel(code, value)).build();
    }


    @Get
    @Mapping("/map")
    @Translator
    public Result map(String code, String value) {

        People people = new People();

        people.setId(code);
        return Result.builder().status(200).data(people).build();
    }

    @Mapping("/demo1")
    @Translator
    public Result demo1() {
        List<People> peopleList = demoService.dictDemo();
        Console.log("---> 字典 & 脱敏 翻译结果：{}", JSONUtil.toJsonStr(peopleList));

        return Result.builder().status(200).data(peopleList).build();
    }

    @Mapping("/demo2")
    @Translator
    public Result demo2() {
        List<Device> deviceList = demoService.enumDemo();
        Console.log("---> 枚举 翻译结果：{}", JSONUtil.toJsonStr(deviceList));
        return Result.builder().status(200).data(deviceList).build();
    }

    @Mapping("/demo3")
    @Translator
    public Result demo3() {
        List<People2> peopleList = demoService.dbDemo();
        Console.log("---> 数据库 翻译结果：{}", JSONUtil.toJsonStr(peopleList));
        return Result.builder().status(200).data(peopleList).build();
    }

    @Mapping("/demo4")
    @Translator
    public Result demo4() {
        List<People3> peopleList = demoService.jsonDemo();
        Console.log("---> json 翻译结果：{}", JSONUtil.toJsonStr(peopleList));
        return Result.builder().status(200).data(peopleList).build();
    }

    @Mapping("/demo5")
    @Translator
    public Result demo5() {
        Result result = demoService.responseNestedMockTest();
        Console.log("---> 响应嵌套数据：{}", JSONUtil.toJsonStr(result));
        return Result.builder().status(200).data(result).build();
    }

    @Mapping("/demo6")
    @Translator
    public Result demo6() {
        List<People> peopleList = demoService.dictDemo2();
        Console.log("---> 字典 & 脱敏 & 摘要提取 翻译结果：{}", JSONUtil.toJsonStr(peopleList));
        return Result.builder().status(200).data(peopleList).build();


    }
}