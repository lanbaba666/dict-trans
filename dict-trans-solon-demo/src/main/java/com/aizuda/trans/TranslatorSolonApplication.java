package com.aizuda.trans;


import lombok.extern.slf4j.Slf4j;
import org.noear.solon.Solon;
import org.noear.solon.annotation.Import;

/**
 * 转换器引导应用程序
 *
 * @author nn200433
 * @date 2022-12-16 11:30:52
 */

@Slf4j
@Import(scanPackages = "org.noear.solon.trans.config")
public class TranslatorSolonApplication {

    public static void main(String[] args) {
        Solon.start(TranslatorSolonApplication.class, args, app -> {
        });
    }

}
