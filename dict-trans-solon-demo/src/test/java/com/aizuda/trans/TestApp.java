package com.aizuda.trans;

import cn.hutool.core.lang.Console;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.noear.solon.test.HttpTester;
import org.noear.solon.test.SolonJUnit4ClassRunner;
import org.noear.solon.test.SolonTest;

import java.io.IOException;

/**
 * @author 等風來再離開
 * @date 2023/6/26 9:43
 **/
@Slf4j
@RunWith(SolonJUnit4ClassRunner.class)
@SolonTest(TranslatorSolonApplication.class)
public class TestApp extends HttpTester {


    @Test
    public void demo1() throws IOException {
        String json = path("/test/demo1").get();
        Console.log("---> 字典 & 脱敏 翻译结果：{}", json);
    }

    @Test
    public void demo2() throws IOException {
        String json = path("/test/demo2").get();
        Console.log("---> 枚举 翻译结果：{}", json);
    }

    @Test
    public void demo3() throws IOException {
        String json = path("/test/demo3").get();
        Console.log("---> 数据库 翻译结果：{}", json);
    }

    @Test
    public void demo4() throws IOException {
        String json = path("/test/demo4").get();
        Console.log("---> json 翻译结果：{}", json);
    }

    @Test
    public void demo5() throws IOException {
        String json = path("/test/demo5").get();
        Console.log("---> 响应嵌套数据：{}", json);
    }

    @Test
    public void demo6() throws IOException {
        String json = path("/test/demo6").get();
        Console.log("---> 字典 & 脱敏 & 摘要提取 翻译结果：{}", json);
    }

}
